#!/usr/bin/env python
import sys

dataset= open(sys.argv[1], "r").read().strip().split("\n\n")
n_corr, n_tokens = 0, 0
for data in dataset:
    for line in data.split("\n"):
        tokens = line.split()
        n_tokens += 1
        if tokens[1] == tokens[2]:
            n_corr += 1
print "%f" % (float(n_corr) / n_tokens)

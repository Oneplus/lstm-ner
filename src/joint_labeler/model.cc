#include "model.h"
#include <boost/assert.hpp>

ValidatorI::~ValidatorI() {}

void ValidatorI::get_possible_tags(tags_t& possible_tags) const {
  BOOST_ASSERT_MSG(false, "Override");
}


void ValidatorI::get_possible_tags(unsigned prev, tags_t& possible_tags) const {
  BOOST_ASSERT_MSG(false, "Override");
}


SimpleValidator::SimpleValidator(size_t n) : n_tags(n) {

}


void SimpleValidator::get_possible_tags(tags_t& possible_tags) const {
  possible_tags.resize(n_tags);
  for (unsigned i = 0; i < n_tags; ++i) { possible_tags[i] = i; }
}


void SimpleValidator::get_possible_tags(unsigned prev, tags_t& possible_tags) const {
  possible_tags.resize(n_tags);
  for (unsigned i = 0; i < n_tags; ++i) { possible_tags[i] = i; }
}


NERValidator::NERValidator(const std::vector<std::string>& id_to_tags)
  : id_to_netags(id_to_tags) {

}


void NERValidator::get_possible_tags(tags_t& possible_tags) const {
  for (unsigned i = 0; i < id_to_netags.size(); ++i) {
    const std::string& str = id_to_netags[i];
    if (str[0] == 'O' || str[0] == 'o' || str[0] == 'B' || str[0] == 'b') {
      possible_tags.push_back(i);
    }
  }
}


void NERValidator::get_possible_tags(unsigned prev, tags_t& possible_tags) const {
  BOOST_ASSERT_MSG(prev < id_to_netags.size(), "Previous label id not in range.");
  const std::string& prev_str = id_to_netags[prev];

  if (prev_str[0] == 'O') {
    for (unsigned i = 0; i < id_to_netags.size(); ++i) {
      const std::string& str = id_to_netags[i];
      if (str[0] == 'I' || str[0] == 'i') {
        continue;
      }
      possible_tags.push_back(i);
    }
  } else if (prev_str[0] == 'B') {
    for (unsigned i = 0; i < id_to_netags.size(); ++i) {
      const std::string& str = id_to_netags[i];
      if (str[0] == 'I' && str.substr(1) != prev_str.substr(1)) {
        continue;
      }
      possible_tags.push_back(i);
    }
  } else if (prev_str[0] == 'I') {
    for (unsigned i = 0; i < id_to_netags.size(); ++i) {
      const std::string& str = id_to_netags[i];
      if (str[0] == 'I' && str.substr(1) != prev_str.substr(1)) {
        continue;
      }
      possible_tags.push_back(i);
    }
  }
}


unsigned SequenceLabelingModel::get_best_scored_label(const std::vector<float>& scores,
  const std::vector<unsigned>& possible_labels) {
  float best_score = scores[possible_labels[0]];
  unsigned best_lid = 0;
  for (unsigned j = 1; j < possible_labels.size(); ++j) {
    if (best_score < scores[possible_labels[j]]) {
      best_score = scores[possible_labels[j]];
      best_lid = possible_labels[j];
    }
  }
  return best_lid;
}


unsigned SequenceLabelingModel::get_best_scored_label(const std::vector<float>& scores) {
  float best_score = scores[0];
  unsigned best = 0;
  for (unsigned j = 1; j < scores.size(); ++j) {
    if (best_score < scores[j]) {
      best_score = scores[j];
      best = j;
    }
  }
  return best;
}


BiLSTMMultiTasksModel::BiLSTMMultiTasksModel(cnn::Model* model,
  size_t size_word,
  size_t dim_word,
  size_t size_pretrained_word,
  size_t dim_pretrained_word,
  size_t dim_lstm_input,
  size_t n_lstm_layers,
  size_t dim_hidden,
  const std::vector<size_t>& size_labels,
  const std::unordered_map<unsigned, std::vector<float>>& pretrained_embedding): 
  input_layer(model, size_word, dim_word, 0, 0, size_pretrained_word, dim_pretrained_word, 
  dim_lstm_input, pretrained_embedding),
  bilstm_layer(model, n_lstm_layers, dim_lstm_input, dim_hidden),
  merge2_layer(model, dim_hidden, dim_hidden, dim_hidden),
  dense_layers(),
  softmax_layers(),
  pretrained(pretrained_embedding) {
  for (auto& size_label : size_labels) {
    dense_layers.push_back(DenseLayer(model, dim_hidden, dim_hidden));
    softmax_layers.push_back(SoftmaxLayer(model, dim_hidden, size_label));
  }
}


void BiLSTMMultiTasksModel::log_probability(cnn::ComputationGraph* hg,
  const std::vector<unsigned>& raw_sentence,
  const std::vector<unsigned>& sentence,
  const std::vector<std::string>& sentence_str,
  const std::vector<unsigned>& tasks,
  const std::vector<tags_t>& corrects,
  const std::vector<ValidatorI*>& validators,
  std::vector<double>& n_corrects,
  std::vector<double>& n_items,
  std::vector<tags_t>& predicts) {

  unsigned len = sentence.size();

  bool training_mode = true;
  for (auto& task_id : tasks) {
    training_mode = (training_mode && corrects[task_id].size() == len);
  }
  
  std::vector<cnn::expr::Expression> exprs(len);
  for (unsigned i = 0; i < len; ++i) {
    auto wid = sentence[i];
    auto pre_wid = raw_sentence[i];
    if (!pretrained.count(pre_wid)) { pre_wid = 0; }
    exprs[i] = input_layer.add_input(hg, wid, 0, pre_wid);
  }

  bilstm_layer.new_graph(hg);
  bilstm_layer.add_inputs(hg, exprs);

  std::vector<BidirectionalLSTMLayer::Output> hidden1;
  bilstm_layer.get_outputs(hg, hidden1);

  std::vector<cnn::expr::Expression> merged1(len);
  for (unsigned i = 0; i < len; ++i) {
    merged1[i] = cnn::expr::rectify(merge2_layer.get_output(
      hg, hidden1[i].first, hidden1[i].second));
  }

  std::vector<cnn::expr::Expression> log_probs;
  unsigned prev_id = 0;
  for (auto& t : tasks) {
    const tags_t& correct = corrects[t];
    const ValidatorI* validator = validators[t];

    predicts[t].clear();
    n_items[t] += len;
    for (unsigned i = 0; i < len; ++i) {
      std::vector<unsigned> possible_tags;
      if (i == 0) {
        validator->get_possible_tags(possible_tags);
      } else {
        validator->get_possible_tags(prev_id, possible_tags);
      }

      BOOST_ASSERT_MSG(possible_tags.size() > 0, "No possible tags, unexpected!");

      // perform linear transformation and rectify.
      /*cnn::expr::Expression softmax_scores = softmax_layers[t].get_output(hg, 
        cnn::expr::rectify(dense_layers[t].get_output(hg, merged1[i]))
        );*/
      // according to C&W 2008, a linear transform is directly performed on the hidden layer.
      cnn::expr::Expression softmax_scores = softmax_layers[t].get_output(hg, merged1[i]);
      hg->incremental_forward();
      std::vector<float> scores = cnn::as_vector(hg->get_value(softmax_scores));

      unsigned best_id = get_best_scored_label(scores, possible_tags);
      unsigned id = best_id;
      if (training_mode) {
        BOOST_ASSERT_MSG(correct[i] < scores.size(), "Correct is greater than output");
        id = correct[i];
        if (best_id == correct[i]) { ++n_corrects[t]; }
      }

      log_probs.push_back(cnn::expr::pick(softmax_scores, id));
      predicts[t].push_back(id);
      prev_id = id;
    }
  }

  cnn::expr::Expression tot_neglogprob = -cnn::expr::sum(log_probs);
  assert(tot_neglogprob.pg != nullptr);
}

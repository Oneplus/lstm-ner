#include "utils.h"
#include "logging.h"
#include "corpus.h"
#include <iostream>
#include <cstdio>
#include <fstream>
#include <boost/assert.hpp>
#include <boost/algorithm/string.hpp>

Corpus::Corpus() {}


void Corpus::load_training_data(const std::string& filename) {
  std::ifstream train_file(filename);
  std::string line;

  word_to_id[Corpus::BAD0] = 0; id_to_word[0] = Corpus::BAD0;
  word_to_id[Corpus::UNK] = 1;  id_to_word[1] = Corpus::UNK;

  BOOST_ASSERT_MSG(max_word == 0, "max_word is set before loading training data!");
  max_word = 2;

  std::vector<unsigned> current_sentence;
  std::vector<unsigned> current_postag;
  std::vector<unsigned> current_netag;

  unsigned sid = 0;
  while (std::getline(train_file, line)) {
    replace_string_in_place(line, "-RRB-", "_RRB_");
    replace_string_in_place(line, "-LRB-", "_LRB_");

    boost::algorithm::trim(line);
    if (line.empty()) {
      if (current_sentence.size() == 0) { continue; }
      train_sentences[sid] = current_sentence;
      train_postags[sid] = current_postag;
      train_netags[sid] = current_netag;
      sid++;
      n_train = sid;
      current_sentence.clear();
      current_postag.clear();
      current_netag.clear();
    } else {
      std::vector<std::string> items;
      boost::algorithm::split(items, line, boost::is_any_of("\t "), boost::token_compress_on);
      BOOST_ASSERT_MSG(items.size() == 4, "Ill formated CoNLL data");
      const std::string& word = items[0];
      const std::string& postag = items[1];
      const std::string& netag = items[3];
      unsigned nid = 0; unsigned pid = 0;  bool found = false;
      
      add(word, max_word, word_to_id, id_to_word);

      found = find(postag, id_to_postag, pid);
      if (!found) { 
        id_to_postag.push_back(postag);
        pid = id_to_postag.size() - 1;
      }

      found = find(netag, id_to_netag, nid);
      if (!found) {
        id_to_netag.push_back(netag);
        nid = id_to_netag.size() - 1;
      }

      current_sentence.push_back(word_to_id[word]);
      current_postag.push_back(pid);
      current_netag.push_back(nid);
    }
  }

  if (current_sentence.size() > 0) {
    train_sentences[sid] = current_sentence;
    train_postags[sid] = current_postag;
    train_netags[sid] = current_netag;
    n_train = sid + 1;
  }

  train_file.close();
  _INFO << "finish loading training data.";
  n_postags = id_to_postag.size();
  n_netags = id_to_netag.size();
  stat();
}


void Corpus::load_devel_data(const std::string& filename) {
  std::ifstream devel_file(filename);
  std::string line;

  BOOST_ASSERT_MSG(max_word > 3, "max_word is not set before loading development data!");
  std::vector<unsigned> current_sentence;
  std::vector<std::string> current_sentence_str;
  std::vector<unsigned> current_postag;
  std::vector<unsigned> current_netag;

  unsigned sid = 0;
  while (std::getline(devel_file, line)) {
    replace_string_in_place(line, "-RRB-", "_RRB_");
    replace_string_in_place(line, "-LRB-", "_LRB_");

    if (line.empty()) {
      if (current_sentence.size() == 0) {
        // To handle the leading empty line.
        continue;
      }
      devel_sentences[sid] = current_sentence;
      devel_sentences_str[sid] = current_sentence_str;
      devel_postags[sid] = current_postag;
      devel_netags[sid] = current_netag;

      sid++;
      n_devel = sid;
      current_sentence.clear();
      current_sentence_str.clear();
      current_postag.clear();
      current_netag.clear();
    } else {
      boost::algorithm::trim(line);
      std::vector<std::string> items;
      boost::algorithm::split(items, line, boost::is_any_of("\t "), boost::token_compress_on);

      BOOST_ASSERT_MSG(items.size() == 4, "Ill formated CoNLL data");
      const std::string& word = items[0];
      const std::string& postag = items[1];
      const std::string& netag = items[3];
      current_sentence_str.push_back(word);
      unsigned nid = 0; unsigned pid = 0;

      add(word, max_word, word_to_id, id_to_word);
      
      if (!find(postag, id_to_postag, pid)) {
        BOOST_ASSERT_MSG(false, "Unknow postag in development data.");
      }

      if (!find(netag, id_to_netag, nid)) {
        BOOST_ASSERT_MSG(false, "Unknow netag in development data.");
      }

      current_sentence.push_back(word_to_id[word]);
      current_sentence_str.push_back(word);
      current_postag.push_back(pid);
      current_netag.push_back(nid);
    }
  }

  if (current_sentence.size() > 0) {
    devel_sentences[sid] = current_sentence;
    devel_sentences_str[sid] = current_sentence_str;
    devel_postags[sid] = current_postag;
    devel_netags[sid] = current_netag;
    n_devel = sid + 1;
  }
}


void Corpus::stat() {
  _INFO << "number of postags: " << id_to_postag.size();
  _INFO << "postag indexing ...";
  for (auto& p : id_to_postag) { _INFO << "- " << p; }
  _INFO << "number of netags: " << id_to_netag.size();
  _INFO << "netag indexing ...";
  for (auto& n : id_to_netag) { _INFO << "- " << n; }
  _INFO << "max id of words: " << max_word;
}


unsigned Corpus::get_or_add_word(const std::string& word) {
  unsigned payload;
  if (!find(word, word_to_id, payload)) {
    add(word, max_word, word_to_id, id_to_word);
    return word_to_id[word];
  }
  return payload;
}

void Corpus::get_vocabulary_and_singletons(std::set<unsigned>& vocabulary,
  std::set<unsigned>& singletons) {
  std::map<unsigned, unsigned> counter;
  for (auto& payload : train_sentences) {
    for (auto& word : payload.second) { vocabulary.insert(word); ++counter[word]; }
  }
  for (auto& payload : counter) {
    if (payload.second == 1) { singletons.insert(payload.first); }
  }
}

#include <iostream>
#include <fstream>
#include <cstdio>
#include <chrono>
#include "logging.h"
#include "training_utils.h"
#include "joint_labeler/model.h"
#include "joint_labeler/corpus.h"
#include "cnn/training.h"
#include <boost/assert.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

namespace po = boost::program_options;

Corpus corpus = Corpus();

void init_command_line(int argc, char* argv[], po::variables_map* conf) {
  po::options_description opts("Configuration options");
  opts.add_options()
    ("graph", po::value<std::string>()->default_value("bilstm"), "The type of graph, avaliable [joint, ml].")
    ("optimizer", po::value<std::string>()->default_value("simple_sgd"), "The optimizer.")
    ("training_data,T", po::value<std::string>(), "Training corpus.")
    ("dev_data,d", po::value<std::string>(), "Development corpus")
    ("pretrained,w", po::value<std::string>(), "Pretrained word embeddings")
    ("unk_strategy,o", po::value<unsigned>()->default_value(1), "Unknown word strategy: 1 = singletons become UNK with probability unk_prob")
    ("unk_prob,u", po::value<double>()->default_value(0.2), "Probably with which to replace singletons with UNK in training data")
    ("model,m", po::value<std::string>(), "Load saved model from this file")
    ("train,t", "Should training be run?")
    ("layers", po::value<unsigned>()->default_value(2), "number of LSTM layers")
    ("word_dim", po::value<unsigned>()->default_value(32), "input embedding size")
    ("pretrained_dim", po::value<unsigned>()->default_value(50), "pretrained input dimension")
    ("hidden_dim", po::value<unsigned>()->default_value(64), "hidden dimension")
    ("lstm_input_dim", po::value<unsigned>()->default_value(60), "LSTM input dimension")
    ("label_dim", po::value<unsigned>()->default_value(16), "label embedding size")
    ("maxiter", po::value<unsigned>()->default_value(10), "Max number of iterations.")
    ("conlleval", po::value<std::string>()->default_value("./conlleval.sh"), "config path to the conlleval script")
    ("verbose,v", "verbose log")
    ("help,h", "Help");

  po::options_description dcmdline_options;
  dcmdline_options.add(opts);
  po::store(parse_command_line(argc, argv, dcmdline_options), *conf);

  if (conf->count("help")) {
    std::cerr << dcmdline_options << std::endl;
    exit(1);
  }
  init_boost_log(conf->count("verbose"));
  if (conf->count("training_data") == 0) {
    _ERROR << "Please specify --training_data (-T): "
      << "this is required to determine the vocabulary mapping, even if the parser is used in prediction mode.";
    exit(1);
  }
}


std::string get_model_name(const po::variables_map& conf) {
  std::ostringstream os;
  os << "joint_" << conf["graph"].as<std::string>()
    << "_" << conf["layers"].as<unsigned>()
    << "_" << conf["word_dim"].as<unsigned>()
    << "_" << conf["label_dim"].as<unsigned>()
    << "_" << conf["lstm_input_dim"].as<unsigned>()
    << "_" << conf["hidden_dim"].as<unsigned>();
#ifndef _MSC_VER
  os << "-" << getpid() << ".params";
#endif
  return os.str();
}


double script(const po::variables_map& conf,
  const std::string& tmp_output) {
#ifndef _MSC_VER
  std::string cmd = conf["conlleval"].as<std::string>() + " " + tmp_output;
  _TRACE << "Running: " << cmd << std::endl;
  FILE* pipe = popen(cmd.c_str(), "r");
  if (!pipe) {
    return 0.;
  }
  char buffer[128];
  std::string result = "";
  while (!feof(pipe)) {
    if (fgets(buffer, 128, pipe) != NULL) { result += buffer; }
  }
  pclose(pipe);

  std::stringstream S(result);
  std::string token;
  while (S >> token) {
    boost::algorithm::trim(token);
    return boost::lexical_cast<double>(token);
  }
#else
  return 1.;
#endif
  return 0.;
}

double evaluate(const po::variables_map& conf,
  SequenceLabelingModel& engine,
  const std::string& tmp_output,
  const std::set<unsigned>& training_vocab) {
  auto kUNK = corpus.get_or_add_word(Corpus::UNK);
  double n_total = 0;
  // validator.
  std::vector<ValidatorI*> validators;
  validators.push_back(new NERValidator(corpus.id_to_netag));
  validators.push_back(new SimpleValidator(corpus.n_postags));
  std::vector<unsigned> tasks = { kNE, kPOS };

  auto t_start = std::chrono::high_resolution_clock::now();
  
  std::vector<tags_t> dummy_correct(2);
  std::vector<double> dummy_n_correct(2);
  std::vector<double> dummy_n_item(2);
  std::vector<tags_t> predict(2);

  std::ofstream ofs(tmp_output);
  for (unsigned sid = 0; sid < corpus.n_devel; ++sid) {
    const std::vector<unsigned>& raw_sentence = corpus.devel_sentences[sid];
    const std::vector<unsigned>& postag = corpus.devel_postags[sid];
    const std::vector<unsigned>& netag = corpus.devel_netags[sid];
    const std::vector<std::string>& sentence_str = corpus.devel_sentences_str[sid];

    unsigned len = raw_sentence.size();

    std::vector<unsigned> sentence = raw_sentence;
    for (auto& w : sentence) {
      if (training_vocab.count(w) == 0) w = kUNK;
    }

    BOOST_ASSERT_MSG(len == postag.size(), "Unequal sentence and gold postag length");
    BOOST_ASSERT_MSG(len == netag.size(), "Unequal sentence and gold netag length");

    cnn::ComputationGraph hg;
    engine.log_probability(&hg, raw_sentence, sentence, sentence_str,
      tasks, dummy_correct, validators, dummy_n_correct, dummy_n_item, predict);

    BOOST_ASSERT_MSG(len == predict[kPOS].size(), "Unequal sentence and predict netag length");
    BOOST_ASSERT_MSG(len == predict[kNE].size(), "Unequal sentence and predict postag length");
    n_total += sentence.size();
    for (unsigned i = 0; i < sentence.size(); ++i) {
      ofs << sentence_str[i] << " " <<
        corpus.id_to_postag[postag[i]] << " " <<
        corpus.id_to_postag[predict[kPOS][i]] << " " <<
        corpus.id_to_netag[netag[i]] << " " <<
        corpus.id_to_netag[predict[kNE][i]] << std::endl;
    }
    ofs << std::endl;
  }
  ofs.close();
  auto t_end = std::chrono::high_resolution_clock::now();
  double f_score = script(conf, tmp_output);
  _INFO << "TEST f-score: " << f_score <<
    " [" << corpus.n_devel <<
    " sents in " << std::chrono::duration<double, std::milli>(t_end - t_start).count() << " ms]";
  for (unsigned i = 0; i < validators.size(); ++i) { delete validators[i]; }
  return f_score;
}


void train(const po::variables_map& conf, cnn::Model& model,
  SequenceLabelingModel& engine, const std::string& model_name,
  const std::string& tmp_output,
  const std::set<unsigned>& vocabulary,
  const std::set<unsigned>& singletons) {
  _INFO << "start joint training ...";
  // Setup the trainer.
  cnn::Trainer* trainer = get_trainer(conf, &model);

  // Order for shuffle.
  std::vector<unsigned> order(corpus.n_train);
  for (unsigned i = 0; i < corpus.n_train; ++i) { order[i] = i; }

  unsigned kUNK = corpus.get_or_add_word(Corpus::UNK);
  auto maxiter = conf["maxiter"].as<unsigned>();
  // counter.
  double n_seen = 0;
  double n_corr_postag = 0, n_corr_netag = 0, n_tokens = 0, llh = 0, batch_llh = 0;
  std::vector<double> batch_n_corrects(2);
  std::vector<double> batch_n_tokens(2);

  int logc = 0;
  _INFO << "number of training instances: " << corpus.n_train;
  _INFO << "going to train " << maxiter << " iterations.";

  auto unk_strategy = conf["unk_strategy"].as<unsigned>();
  auto unk_prob = conf["unk_prob"].as<double>();
  double best_f_score = 0.;

  std::vector<ValidatorI*> validators;
  validators.push_back(new NERValidator(corpus.id_to_netag));
  validators.push_back(new SimpleValidator(corpus.n_postags));

  for (unsigned iter = 0; iter < maxiter; ++iter) {
    _INFO << "start of iteration #" << iter << ", training data is shuffled.";
    std::shuffle(order.begin(), order.end(), (*cnn::rndeng));

    for (unsigned i = 0; i < order.size(); ++i) {
      auto sid = order[i];
      const std::vector<unsigned>& raw_sentence = corpus.train_sentences[sid];
      const std::vector<unsigned>& postag = corpus.train_postags[sid];
      const std::vector<unsigned>& netag = corpus.train_netags[sid];

      std::vector<unsigned> sentence = raw_sentence;
      if (unk_strategy == 1) {
        for (auto& w : sentence) {
          if (singletons.count(w) && cnn::rand01() < unk_prob) { w = kUNK; }
        }
      }

      double lp;
      {
        cnn::ComputationGraph hg;
        std::vector<tags_t> corrects = { netag, postag };
        std::vector<unsigned> tasks = { kNE, kPOS };
        std::vector<tags_t> predicts(2);
        engine.log_probability(&hg, raw_sentence, sentence, std::vector<std::string>(),
          tasks, corrects, validators, batch_n_corrects, batch_n_tokens, predicts);

        lp = cnn::as_scalar(hg.incremental_forward());
        BOOST_ASSERT_MSG(lp >= 0, "Log prob < 0 on sentence");
        hg.backward();
        trainer->update(1.);
      }

      llh += lp; batch_llh += lp;
      n_seen += 1;
      ++logc;
      n_tokens += sentence.size();

      if (logc % 100 == 0) {
        trainer->status();
        _INFO << "iter (batch) #" << iter << " (epoch " << n_seen / corpus.n_train
          << ") llh: " << batch_llh << " ppl: " << exp(batch_llh / batch_n_tokens[kPOS])
          << " pos err: " << (batch_n_tokens[kPOS] - batch_n_corrects[kPOS]) / batch_n_tokens[kPOS]
          << " ne err: " << (batch_n_tokens[kNE] - batch_n_corrects[kNE]) / batch_n_tokens[kNE];

        n_corr_postag += batch_n_corrects[kPOS];
        n_corr_netag += batch_n_corrects[kNE];
        batch_llh = 0;
        std::fill(batch_n_tokens.begin(), batch_n_tokens.end(), 0);
        std::fill(batch_n_corrects.begin(), batch_n_corrects.end(), 0);
      }

      if (logc % 2500 == 0) {
        double f_score = evaluate(conf, engine, tmp_output, vocabulary);
        if (f_score > best_f_score) {
          best_f_score = f_score;
          _INFO << "new best record " << best_f_score << " is achieved, model updated.";
          std::ofstream out(model_name);
          boost::archive::text_oarchive oa(out);
          oa << model;
        }
      }
    }
    _INFO << "iteration #" << iter + 1 << " (epoch " << n_seen / corpus.n_train
      << ") llh: " << llh << " ppl: " << exp(llh / n_tokens)
      << " pos err: " << (n_tokens - n_corr_postag) / n_tokens
      << " ne err: " << (n_tokens - n_corr_netag) / n_tokens;
    llh = n_tokens = n_corr_postag = n_corr_netag = 0.;

    double f_score = evaluate(conf, engine, tmp_output, vocabulary);
    if (f_score > best_f_score) {
      best_f_score = f_score;
      _INFO << "new best record " << best_f_score << " is achieved, model updated.";
      std::ofstream out(model_name);
      boost::archive::text_oarchive oa(out);
      oa << model;
    }
    if (conf["optimizer"].as<std::string>() == "simple_sgd" || conf["optimizer"].as<std::string>() == "momentum_sgd") {
      trainer->update_epoch();
    }
  }
  for (unsigned i = 0; i < validators.size(); ++i) { delete validators[i]; }
  delete trainer;
}


int main(int argc, char* argv[]) {
  cnn::Initialize(argc, argv, 1234);
  std::cerr << "command:";
  for (unsigned i = 0; i < static_cast<unsigned>(argc); ++i) std::cerr << ' ' << argv[i];
  std::cerr << std::endl;

  po::variables_map conf;
  init_command_line(argc, argv, &conf);

  if (conf["unk_strategy"].as<unsigned>() == 1) {
    _INFO << "unknown word strategy: STOCHASTIC REPLACEMENT";
  } else {
    _INFO << "unknown word strategy: NO REPLACEMENT";
  }

  std::string model_name;
  if (conf.count("train")) {
    model_name = get_model_name(conf);
    _INFO << "going to write parameters to file: " << model_name;
  } else {
    model_name = conf["model"].as<std::string>();
    _INFO << "going to load parameters from file: " << model_name;
  }
  corpus.load_training_data(conf["training_data"].as<std::string>());

  std::set<unsigned> training_vocab, singletons;
  corpus.get_vocabulary_and_singletons(training_vocab, singletons);

  std::unordered_map<unsigned, std::vector<float>> pretrained;
  if (conf.count("pretrained")) {
    load_pretrained_word_embedding(conf["pretrained"].as<std::string>(),
      conf["pretrained_dim"].as<unsigned>(), pretrained, corpus);
    _INFO << "pre-trained word embedding is loaded.";
  }
  cnn::Model model;
  SequenceLabelingModel* engine = nullptr;
  _INFO << "building " << conf["graph"].as<std::string>();
  if (conf["graph"].as<std::string>() == "bilstm") {
    engine = new BiLSTMMultiTasksModel(&model, corpus.max_word + 1, conf["word_dim"].as<unsigned>(),
      corpus.max_word + 1, conf["pretrained_dim"].as<unsigned>(),
      conf["lstm_input_dim"].as<unsigned>(),
      conf["layers"].as<unsigned>(),
      conf["hidden_dim"].as<unsigned>(),
      { corpus.n_netags, corpus.n_postags },
      pretrained);
  } else {
    _ERROR << "Unknown graph type:" << conf["graph"].as<std::string>();
    exit(1);
  }

  corpus.load_devel_data(conf["dev_data"].as<std::string>());
  _INFO << "loaded " << corpus.n_devel << " devel sentences.";
#ifdef _MSC_VER
  std::string tmp_output = "lstm.jposner.evaluator";
#else
  std::string tmp_output = "/tmp/lstm.jposner.evaluator." + boost::lexical_cast<std::string>(getpid());
#endif
  if (conf.count("train")) {
    train(conf, model, *engine, model_name, tmp_output, training_vocab, singletons);
  }

  std::ifstream in(model_name);
  boost::archive::text_iarchive ia(in);
  ia >> model;
  evaluate(conf, *engine, tmp_output, training_vocab);
  return 0;
}

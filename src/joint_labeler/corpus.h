#ifndef __CORPUS_H__
#define __CORPUS_H__

#include <iostream>
#include <map>
#include <vector>
#include "utils.h"


struct Corpus : public CorpusI {
  typedef std::map<std::string, unsigned> StringToIdMap;
  typedef std::map<unsigned, std::string> IdToStringMap;

  std::map<unsigned, std::vector<unsigned>> train_sentences;
  std::map<unsigned, std::vector<unsigned>> train_postags;
  std::map<unsigned, std::vector<unsigned>> train_netags;

  std::map<unsigned, std::vector<unsigned>> devel_sentences;
  std::map<unsigned, std::vector<std::string>> devel_sentences_str;
  std::map<unsigned, std::vector<unsigned>> devel_postags;
  std::map<unsigned, std::vector<unsigned>> devel_netags;

  unsigned n_train;
  unsigned n_devel;

  unsigned n_postags;
  unsigned n_netags;

  unsigned max_word;
  StringToIdMap word_to_id;
  IdToStringMap id_to_word;
  std::vector<std::string> id_to_postag;
  std::vector<std::string> id_to_netag;

  Corpus();
  void load_training_data(const std::string& filename);
  void load_devel_data(const std::string& filename);
  void stat();
  unsigned get_or_add_word(const std::string& word);
  void get_vocabulary_and_singletons(std::set<unsigned>&, std::set<unsigned>&);
};

#endif  //  end for __CORPUS_H__
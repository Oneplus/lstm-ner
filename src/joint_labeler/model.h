#ifndef __MODEL_H__
#define __MODEL_H__

#include "layer.h"

typedef std::vector<unsigned> tags_t;
#define kNE   0
#define kPOS  1

struct ValidatorI {
  virtual ~ValidatorI();
  virtual void get_possible_tags(tags_t& possible_tags) const;
  virtual void get_possible_tags(unsigned prev, tags_t& possible_tags) const;
};


struct NERValidator : public ValidatorI {
  const std::vector<std::string>& id_to_netags;
  NERValidator(const std::vector<std::string>& id_to_netags);

  void get_possible_tags(tags_t& possible_tags) const;
  void get_possible_tags(unsigned prev, tags_t& possible_tags) const;
};


struct SimpleValidator : public ValidatorI {
  size_t n_tags;
  SimpleValidator(size_t n_tags);

  void get_possible_tags(tags_t& possible_tags) const;
  void get_possible_tags(unsigned prev, tags_t& possible_tags) const;
};


struct SequenceLabelingModel {
  virtual void log_probability(cnn::ComputationGraph* hg,
    const std::vector<unsigned>& raw_sentence,
    const std::vector<unsigned>& sentence,
    const std::vector<std::string>& sentence_str,
    const std::vector<unsigned>& tasks,
    const std::vector<tags_t>& corrects,
    const std::vector<ValidatorI*>& validators,
    std::vector<double>& n_corrects,
    std::vector<double>& n_items,
    std::vector<tags_t>& predicts) = 0;

  virtual void explain(std::ostream& os,
    const std::map<unsigned, std::string>& id_to_word,
    const std::vector<std::string>& id_to_postag,
    const std::vector<std::string>& id_to_label) {}

  unsigned get_best_scored_label(const std::vector<float>& scores);
  unsigned get_best_scored_label(const std::vector<float>& scores, const tags_t& possible_tags);
};


struct BiLSTMMultiTasksModel: public SequenceLabelingModel {
  StaticInputLayer input_layer;
  BidirectionalLSTMLayer bilstm_layer;
  Merge2Layer merge2_layer;
  std::vector<DenseLayer> dense_layers;
  std::vector<SoftmaxLayer> softmax_layers;
  const std::unordered_map<unsigned, std::vector<float>>& pretrained;

  BiLSTMMultiTasksModel(cnn::Model* model,
    size_t size_word,
    size_t dim_word,
    size_t size_pretrained_word,
    size_t dim_pretrained_word,
    size_t dim_lstm_input,
    size_t n_lstm_layers,
    size_t dim_hidden,
    const std::vector<size_t>& size_labels,
    const std::unordered_map<unsigned, std::vector<float>>& pretrained_embedding);

  void log_probability(cnn::ComputationGraph* hg,
    const std::vector<unsigned>& raw_sentence,
    const std::vector<unsigned>& sentence,
    const std::vector<std::string>& sentence_str,
    const std::vector<unsigned>& tasks,
    const std::vector<tags_t>& corrects,
    const std::vector<ValidatorI*>& validators,
    std::vector<double>& n_corrects,
    std::vector<double>& n_items,
    std::vector<tags_t>& predicts);
};

#endif  //  end for __MODEL_H__

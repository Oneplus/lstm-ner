#ifndef __CORPUS_H__
#define __CORPUS_H__

#include <iostream>
#include <map>
#include <set>
#include <vector>
#include "utils.h"


struct Corpus : public CorpusI {
  StringToIdMap word_to_id;
  IdToStringMap id_to_word;
  unsigned max_word;
  std::vector<std::vector<std::string>> id_to_output;
  
  std::map<std::string, unsigned> task_to_id;
  std::vector<std::string> id_to_task;


  std::vector<std::map<unsigned, Sentence>> train_sentences, devel_sentences;
  std::vector<std::map<unsigned, Output>> train_outputs, devel_outputs;
  std::vector<std::map<unsigned, RawSentence>> devel_sentenceset;
  
  std::vector<unsigned> n_trains;
  std::vector<unsigned> n_devels;
  std::vector<bool> activated_devels;
  
  unsigned n_tasks;

  Corpus();
  void load_training_data(const std::vector<std::string>& arguments);
  void load_training_data_one_task(const std::string& filename, unsigned task_id);
  void load_devel_data(const std::vector<std::string>& arguments);
  void load_devel_data_one_task(const std::string& filename, unsigned task_id);
  unsigned get_or_add_word(const std::string& word);
  void get_vocabulary_and_singletons(std::set<unsigned>&, std::set<unsigned>&);
};

#endif  //  end for __CORPUS_H__
#include <iostream>
#include <fstream>
#include <cstdio>
#include "corpus.h"
#include "logging.h"
#include <boost/assert.hpp>
#include <boost/algorithm/string.hpp>


Corpus::Corpus() : n_tasks(0), max_word(0) {
}


void Corpus::load_training_data(const std::vector<std::string>& arguments) {
  BOOST_ASSERT_MSG(arguments.size() > 0, "Number of tasks should be greater than zero.");

  n_tasks = arguments.size();
  id_to_output.resize(n_tasks);
  id_to_task.resize(n_tasks);

  train_sentences.resize(n_tasks);
  train_outputs.resize(n_tasks);
  n_trains.resize(n_tasks);
  activated_devels.resize(n_tasks, false);

  unsigned task_id = 0;

  // Initialize the BAD0 and UNK
  add(Corpus::BAD0, max_word, word_to_id, id_to_word);
  add(Corpus::UNK, max_word, word_to_id, id_to_word);

  for (auto& argument : arguments) {
    auto found = argument.find("=");
    BOOST_ASSERT_MSG(found != std::string::npos, "Ill formated");

    std::string task_name = argument.substr(0, found);
    BOOST_ASSERT_MSG(task_to_id.find(task_name) == task_to_id.end(), "One task cann't be registed mutiple times.");
    task_to_id[task_name] = task_id;
    id_to_task[task_id] = task_name;
    _INFO << "Registed " << task_name << " task.";

    std::string filename = argument.substr(found + 1);
    load_training_data_one_task(filename, task_id);

    _INFO << "indexing output for task: " << task_name << " with " << id_to_output[task_id].size();
    for (auto& o : id_to_output[task_id]) { _INFO << " - " << o; }
    ++task_id;
  }
}


void Corpus::load_training_data_one_task(const std::string& filename, unsigned task_id) {
  std::ifstream fin(filename);
  std::string line;
  
  Sentence current_sentence;
  Output current_output;

  unsigned sid = 0;
  std::map<unsigned, Sentence>& train_sentence = train_sentences[task_id];
  std::map<unsigned, Output>& train_output = train_outputs[task_id];

  auto& id_to_output_ = id_to_output[task_id];

  while (std::getline(fin, line)) {
    boost::algorithm::trim(line);
    if (line.size() == 0) {
      train_sentence[sid] = current_sentence;
      train_output[sid] = current_output;
      current_sentence.clear();
      current_output.clear();
      ++sid;
    } else {
      std::vector<std::string> items;
      boost::split(items, line, boost::is_any_of("\t "), boost::token_compress_on);
      BOOST_ASSERT_MSG(items.size() >= 2, "Ill formated input data");
      add(items[0], max_word, word_to_id, id_to_word);
      unsigned wid = word_to_id[items[0]];
      unsigned oid;
      bool found = find(items[1], id_to_output_, oid);
      if (!found) { id_to_output_.push_back(items[1]); oid = id_to_output_.size() - 1; }
      current_sentence.push_back(wid);
      current_output.push_back(oid);
    }
  }
  if (current_sentence.size() > 0) {
    train_sentence[sid] = current_sentence;
    train_output[sid] = current_output;
    ++sid;
  }
  n_trains[task_id] = sid;
  _INFO << "For task " << id_to_task[task_id] << " : loaded " << n_trains[task_id] << " instances";
}


void Corpus::load_devel_data(const std::vector<std::string>& arguments) {
  BOOST_ASSERT_MSG(word_to_id.size() > 2, "word to id is not initialzed before loading devel data.");

  devel_sentences.resize(n_tasks);
  devel_outputs.resize(n_tasks);
  devel_sentenceset.resize(n_tasks);
  n_devels.resize(n_tasks);
  unsigned task_id = 0;
  for (auto& argument : arguments) {
    auto found = argument.find("=");
    BOOST_ASSERT_MSG(found != std::string::npos, "Ill format");

    std::string task_name = argument.substr(0, found);
    BOOST_ASSERT_MSG(task_to_id.find(task_name) != task_to_id.end(), "Not found task");
    task_id = task_to_id[task_name];
    activated_devels[task_id] = true;
    _INFO << "Load " << task_name << " development data.";

    std::string filename = argument.substr(found + 1);
    load_devel_data_one_task(filename, task_id);
  }
}


void Corpus::load_devel_data_one_task(const std::string& filename, unsigned task_id) {
  std::ifstream fin(filename);
  std::string line;

  Sentence current_sentence;
  Output current_output;
  RawSentence current_raw_sentence;
  unsigned sid = 0;

  auto& devel_sentence = devel_sentences[task_id];
  auto& devel_output = devel_outputs[task_id];
  auto& devel_raw_sentence = devel_sentenceset[task_id];
  while (std::getline(fin, line)) {
    boost::algorithm::trim(line);
    if (line.size() == 0) {
      devel_sentence[sid] = current_sentence;
      devel_output[sid] = current_output;
      devel_raw_sentence[sid] = current_raw_sentence;
      current_sentence.clear();
      current_output.clear();
      current_raw_sentence.clear();
      ++sid;
    } else {
      std::vector<std::string> items;
      boost::split(items, line, boost::is_any_of("\t "), boost::token_compress_on);

      current_raw_sentence.push_back(items[0]);
      unsigned payload = 0;
      if (!find(items[1], id_to_output[task_id], payload)) {
        BOOST_ASSERT_MSG(false, "Unknow output type in development data.");
      } else {
        current_output.push_back(payload);
      }

      if (!find(items[0], word_to_id, payload)) { find(Corpus::UNK, word_to_id, payload); } 
      current_sentence.push_back(payload); 
    }
  }
  if (current_sentence.size() > 0) {
    devel_sentence[sid] = current_sentence;
    devel_output[sid] = current_output;
    devel_raw_sentence[sid] = current_raw_sentence;
    ++sid;
  }
  n_devels[task_id] = sid;
}


unsigned Corpus::get_or_add_word(const std::string& word) {
  unsigned payload;
  if (!find(word, word_to_id, payload)) {
    add(word, max_word, word_to_id, id_to_word);
    return word_to_id[word];
  }
  return payload;
}


void Corpus::get_vocabulary_and_singletons(std::set<unsigned>& vocabulary,
  std::set<unsigned>& singletons) {
  std::map<unsigned, unsigned> counter;
  for (auto& train_data : train_sentences) {
    for (auto& payload : train_data) {
      for (auto& word : payload.second) { vocabulary.insert(word); ++counter[word]; }
    }
  }
  for (auto& payload : counter) {
    if (payload.second <= n_tasks) { singletons.insert(payload.first); }
  }
  _INFO << "Collected " << vocabulary.size() << " in-vocabulary words.";
  _INFO << "Collected " << singletons.size() << " singleton words.";
}

#!/usr/bin/env python
from __future__ import print_function
from collections import Counter
from log import get_logger
from crp import Corpus
import numpy as np
import subprocess
import sys
import os
import theano

np.random.seed(1337)  # for reproducibility

#from keras.preprocessing import sequence
#from keras.optimizers import SGD, RMSprop, Adagrad
from keras.utils import np_utils
from keras.models import Graph
from keras.regularizers import l2
from keras.layers.core import RepeatVector, Dense, Activation, TimeDistributedDense
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM

LOG = get_logger(__name__, None, "info")
MODEL_NAME = "ner_{GRAPH}_{USE_POS}_{USE_SPELL}_{LAYERS}_{WORD}_{HIDDEN}_{ACTION}_{LSTM_INPUT}_{POS}_{pid}"
MAX_LEN = 128 # Fit the eng.train, eng.dev and eng.test


def init_command_line(argv):
    """

    :param list[str] argv:
    :return:
    """
    from argparse import ArgumentParser
    usage = "LSTM-NER (Keras version)"
    description = ArgumentParser(usage)
    description.add_argument("--optimizer", default="adam", help="The optimizer")
    description.add_argument("--training_data", "-T", help="The training data.")
    description.add_argument("--dev_data", "-d", help="The development data.")
    description.add_argument("--unk_strategy", "-o", type=int, default=1,
                             help="Unknown word strategy: 1 = singletons become UNK with probability unk_prob.")
    description.add_argument("--unk_prob", "-u", type=float, default=0.2,
                             help="Probably with which to replace singletons with UNK in training data.")
    description.add_argument("--model", "-m", help="Load saved model from this file")
    description.add_argument("--use_pos", "-P", action="store_true", default=False,
                             help="make POS tags visible to parser")
    description.add_argument("--use_spelling", "-S", action="store_true", default=False,
                             help="Use spelling model")
    description.add_argument("--layers", type=int, default=2, help="number of LSTM layers")
    description.add_argument("--hidden_dim", type=int, default=64, help="hidden dimension")
    description.add_argument("--action_dim", type=int, default=16, help="action embedding size")
    description.add_argument("--word_dim", type=int, default=32, help="word embedding size")
    description.add_argument("--pretrained_dim", type=int, default=50, help="pretrained input dimension")
    description.add_argument("--pos_dim", type=int, default=12, help="POS dimension")
    description.add_argument("--lstm_input_dim", type=int, default=60, help="LSTM input dimension")
    description.add_argument("--train", "-t", default=False, action="store_true",
                             help="Should training be run?")
    description.add_argument("--words", "-w", help="Pretrained word embeddings")
    description.add_argument("--conlleval", default="./conlleval", help="config path to the conlleval script")
    description.add_argument("--maxiter", type=int, default=30, help="The max iteration.")
    description.add_argument("--graph", default="plain", help="The graph type {plain,chris}.")
    return description.parse_args(argv)


def load_pretrained_word_embedding(opts, corpus):
    """

    :param argparse.Namespace opts:
    :param Corpus corpus:
    :return:
    """
    LOG.info("Loading pre-trained word embedding from %s" % opts.words)
    embeddings = {}
    try:
        fp = open(opts.words, "r")
        _ = fp.readline()  # skip header
        for line in fp:
            tokens = line.strip().split()
            word, values = tokens[0], np.asarray(map(float, tokens[1:]), dtype=theano.config.floatX)
            embeddings[corpus.get_or_add_word(word)] = values
            dim = len(tokens[1:])
        embeddings[1] = np.zeros((dim,), dtype=theano.config.floatX)
        return embeddings, dim
    except Exception, e:
        LOG.error("Failed to load word embedding from %s" % opts.words)
        raise e


class LSTMSequenceLabelerBuilder(object):
    def __init__(self):
        self.word_dim = 0
        self.word_size = 0
        self.postag_dim = 0
        self.postag_size = 0
        self.label_size = 0
        self.hidden_dim = 0
        self.layers = 1
        self.lstm_input_dim = 0
        self.pretrained_embeddings_dim = 0

    def set_word_size(self, word_size):
        self.word_size = word_size
        return self

    def set_word_dim(self, word_dim):
        self.word_dim = word_dim
        return self

    def set_postag_size(self, postag_size):
        self.postag_size = postag_size
        return self

    def set_postag_dim(self, postag_dim):
        self.postag_dim = postag_dim
        return self

    def set_label_size(self, label_size):
        self.label_size = label_size
        return self

    def set_hidden_dim(self, hidden_dim):
        self.hidden_dim = hidden_dim
        return self

    def set_layers(self, layers):
        self.layers = layers
        return self

    def set_lstm_input_dim(self, lstm_input_dim):
        self.lstm_input_dim = lstm_input_dim
        return self

    def set_pretrained_embeddings_dim(self, embeddings_dim):
        self.pretrained_embeddings_dim = embeddings_dim
        return self

    def _build_lstm_input(self):
        model = Graph()
        model.add_input(name='wid', input_shape=(1,), dtype=int)
        model.add_input(name='pid', input_shape=(1,), dtype=int)
        model.add_input(name="pre_word_emb", input_shape=(None, self.pretrained_embeddings_dim), dtype="float")
        model.add_node(Embedding(self.postag_size, self.postag_dim, init='glorot_uniform', W_regularizer=l2(1e-6)), name="postag_emb", input="pid")
        model.add_node(Embedding(self.word_size, self.word_dim, init='glorot_uniform', W_regularizer=l2(1e-6)), name="word_emb", input="wid")
        return model

    def _lstm(self, dim, go_backwards=False, return_sequences=True):
        return LSTM(dim, go_backwards=go_backwards, return_sequences=return_sequences, inner_activation='sigmoid')

    def build_plain_graph(self):
        model = self._build_lstm_input()
        for i in range(self.layers):
            if i == 0:
                model.add_node(self._lstm(self.hidden_dim), name="fw0", inputs=["word_emb", "postag_emb", "pre_word_emb"])
                model.add_node(self._lstm(self.hidden_dim, True), name="bw0", inputs=["word_emb", "postag_emb", "pre_word_emb"])
            else:
                model.add_node(self._lstm(self.hidden_dim), name="fw%d" % i, input="fw%d" % (i-1))
                model.add_node(self._lstm(self.hidden_dim, True), name="bw%d" % i, input="bw%d" % (i-1))
        model.add_node(TimeDistributedDense(self.label_size, W_regularizer=l2(1e-6)), name="timedistdense",
                       inputs=["fw%d" % (self.layers - 1), "bw%d" % (self.layers - 1)])
        model.add_node(Activation("softmax"), name="softmax", input="timedistdense")
        model.add_output(name="output", input="softmax")
        return model

    def build_chris_graph(self):
        model = self._build_lstm_input()
        model.add_node(TimeDistributedDense(self.lstm_input_dim, W_regularizer=l2(1e-6)), name="word_emb_trans", input="word_emb")
        model.add_node(TimeDistributedDense(self.lstm_input_dim, W_regularizer=l2(1e-6)), name="postag_emb_trans", input="postag_emb")
        model.add_node(TimeDistributedDense(self.lstm_input_dim, W_regularizer=l2(1e-6)), name="pre_word_emb_trans", input="pre_word_emb")
        model.add_node(Activation("relu", W_regularizer=l2(1e-6)), name="lstm_input", merge_mode="sum",
                       inputs=["word_emb_trans", "postag_emb_trans", "pre_word_emb_trans"])
        for i in range(self.layers):
            if i == 0:
                model.add_node(self._lstm(self.hidden_dim), name="fw0", input="lstm_input")
                model.add_node(self._lstm(self.hidden_dim, True), name="bw0", input="lstm_input")
            else:
                model.add_node(self._lstm(self.hidden_dim), name="fw%d" % i, input="fw%d" % (i-1))
                model.add_node(self._lstm(self.hidden_dim, True), name="bw%d" % i, input="bw%d" % (i-1))
        last_fw, last_bw = "fw%d" % (self.layers - 1), "bw%d" % (self.layers - 1)
        model.add_node(TimeDistributedDense(self.hidden_dim, W_regularizer=l2(1e-6)), name=last_fw + "_trans", input=last_fw)
        model.add_node(TimeDistributedDense(self.hidden_dim, W_regularizer=l2(1e-6)), name=last_bw + "_trans", input=last_bw)
        #model.add_node(Activation("relu"), name="lstm_output", merge_mode="sum",
        #               inputs=[last_fw + "_trans", last_bw + "_trans"])
        #model.add_node(TimeDistributedDense(self.label_size), name="timedistdense", input="lstm_output")
        model.add_node(TimeDistributedDense(self.label_size, W_regularizer=l2(1e-6)), name="timedistdense", merge_mode="sum",
                       inputs=[last_fw + "_trans", last_bw + "_trans"])
        model.add_node(Activation("softmax", W_regularizer=l2(1e-6)), name="softmax", input="timedistdense")
        model.add_output(name="output", input="softmax")
        return model

    def build_seq2seq_graph(self):
        model = self._build_lstm_input()
        for i in range(self.layers):
            if i == 0:
                model.add_node(self._lstm(self.hidden_dim), name="fw0", inputs=["word_emb", "postag_emb", "pre_word_emb"])
                model.add_node(self._lstm(self.hidden_dim, True), name="bw0", inputs=["word_emb", "postag_emb", "pre_word_emb"])
            elif i == self.layers - 1:
                model.add_node(self._lstm(self.hidden_dim, False, False), name="fw%d" % i, input="fw%d" % (i-1))
                model.add_node(self._lstm(self.hidden_dim, True, False), name="bw%d" % i, input="bw%d" % (i-1))
            else:
                model.add_node(self._lstm(self.hidden_dim), name="fw%d" % i, input="fw%d" % (i-1))
                model.add_node(self._lstm(self.hidden_dim, True), name="bw%d" % i, input="bw%d" % (i-1))
        model.add_node(Dense(self.hidden_dim, W_regularizer=l2(1e-6)), name="fw_glob", input="fw%d" % (self.layers - 1))
        model.add_node(Dense(self.hidden_dim, W_regularizer=l2(1e-6)), name="bw_glob", input="bw%d" % (self.layers - 1))
        model.add_node(RepeatVector(MAX_LEN), name="glob_input", inputs=["fw_glob", "bw_glob"])
        model.add_node(LSTM(self.hidden_dim, return_sequences=True), name="tgt_lstm", input="glob_input")
        model.add_node(TimeDistributedDense(self.label_size, W_regularizer=l2(1e-6)), name="timedistdense", input="tgt_lstm")
        model.add_node(Activation("softmax"), name="softmax", input="timedistdense")
        model.add_output(name="output", input="softmax")
        return model

    def build_plain_alternative_graph(self):
        model = self._build_lstm_input()
        for i in range(self.layers):
            if i == 0:
                model.add_node(self._lstm(self.hidden_dim), name="fw0", inputs=["word_emb", "postag_emb", "pre_word_emb"])
                model.add_node(self._lstm(self.hidden_dim, True), name="bw0", inputs=["word_emb", "postag_emb", "pre_word_emb"])
            else:
                model.add_node(self._lstm(self.hidden_dim), name="fw%d" % i, input="fw%d" % (i-1))
                model.add_node(self._lstm(self.hidden_dim, True), name="bw%d" % i, input="bw%d" % (i-1))
        model.add_node(self._lstm(self.hidden_dim), name="lstm_output", inputs=["fw%d" % (self.layers - 1), "bw%d" % (self.layers - 1)])
        model.add_node(TimeDistributedDense(self.label_size, W_regularizer=l2(1e-6)), name="timedistdense", input="lstm_output")
        model.add_node(Activation("softmax"), name="softmax", input="timedistdense")
        model.add_output(name="output", input="softmax")
        return model


def get_vocabulary_and_singleton(corpus):
    """

    :param Corpus corpus:
    :return set(int), set(int):
    """
    counter = Counter()
    vocabulary, singleton = set(), set()
    for i, words in corpus.train_sentences.iteritems():
        for word in words:
            vocabulary.add(word)
            counter[word] += 1
    for word, freq in counter.iteritems():
        if freq == 1:
            singleton.add(word)
    return vocabulary, singleton


def get_model_name(opts):
    """

    :param argparse.Namespace opts:
    :return str:
    """
    ret = MODEL_NAME.format(**{
        "GRAPH": opts.graph,
        "USE_POS": opts.use_pos,
        "USE_SPELL": opts.use_spelling,
        "LAYERS": opts.layers,
        "WORD": opts.word_dim,
        "HIDDEN": opts.hidden_dim,
        "ACTION": opts.action_dim,
        "LSTM_INPUT": opts.lstm_input_dim,
        "POS": opts.pos_dim,
        "pid": os.getpid()
    })
    return ret


def get_pretrained_sentence(sentence, pretrained_embeddings, pretrained_embeddings_dim):
    """

    :param list[int] sentence:
    :param dict[int, np.array] pretrained_embeddings:
    :param int pretrained_embeddings_dim:
    :return:
    """
    ZEROS = np.zeros((pretrained_embeddings_dim,))
    return [pretrained_embeddings[word] if word in pretrained_embeddings else ZEROS for word in sentence]


def conlleval(cmd, filename):
    """
    Execute the conlleval script.
    :param str cmd:
    :param str filename:
    :return float:
    """
    LOG.info("Executing %s" % " ".join([cmd, "<", filename]))
    p = subprocess.Popen([cmd], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    output, _ = p.communicate(open(filename, "r").read())
    #output = p.stdout.read()
    for line in output.split("\n"):
        if line.startswith("accuracy:"):
            return float(line.split("FB1:")[1].strip())
    return 0.


def evaluate(opts, corpus, vocabulary, singleton, pretrained_embeddings, model, kUNK):
    """

    :param argparse.Namespace opts:
    :param Corpus corpus:
    :param set(int) vocabulary:
    :param set(int) singleton:
    :param dict[int, np.array] pretrained_embeddings:
    :param Graph model:
    :param int kUNK:
    :return float:
    """
    filename = "tmp.output.for.conlleval.%d" % os.getpid()
    fpo = open(filename, "w")
    for i in range(corpus.n_dev):
        sentence, postags, labels = corpus.dev_sentences[i], corpus.dev_postags[i], corpus.dev_labels[i]
        if not opts.use_spelling:
            sentence = [word if word in vocabulary else kUNK for word in sentence]
        pretrained_sentence = get_pretrained_sentence(corpus.dev_sentences[i], pretrained_embeddings,
                                                      opts.pretrained_embeddings_dim)
        res = model.predict({"wid": np.array([sentence]),
                             "pid": np.array([postags]),
                             "pre_word_emb": np.array([pretrained_sentence])}, verbose=0)
        for output in res["output"].tolist():
            for j, categorical_probas in enumerate(output):
                tag_index = np.argmax(categorical_probas)
                if j >= len(sentence):
                    break
                print("%s %s %s %s" % (corpus.int_to_word.get(sentence[j], "**ERROR**"),
                                       corpus.int_to_pos.get(postags[j], "**ERROR**"),
                                       corpus.actions_to_int[labels[j]], corpus.actions_to_int[tag_index]), file=fpo)
            print("", file=fpo)
    fpo.close()
    return conlleval(opts.conlleval, filename)


def train(opts, fname, corpus, vocabulary, singleton, pretrained_embeddings, model):
    """

    :param argparse.Namespace opts:
    :param str fname:
    :param Corpus corpus:
    :param set(int) vocabulary:
    :param set(int) singleton:
    :param dict[int, np.array] pretrained_embeddings:
    :param Graph model:
    :return:
    """
    kUNK = corpus.get_or_add_word(corpus.UNK)
    order = np.arange(corpus.n_train)

    LOG.info("Going to train %d iterations." % opts.maxiter)
    best_f_score = -float("inf")
    for iteration in range(opts.maxiter):
        LOG.info("Order shuffled.")
        np.random.shuffle(order)
        loss, accuracy = 0., 0.
        for n_processed, i in enumerate(order):
            if opts.unk_strategy == 1:
                sentence = [kUNK if word in singleton and np.random.rand() < opts.unk_prob else word for word in corpus.train_sentences[i]]
                sentence = np.asarray(sentence, dtype="int32")
            else:
                sentence = np.asarray(corpus.train_sentences[i], dtype="int32")
            postags = np.asarray(corpus.train_postags[i], dtype="int32")
            labels = np_utils.to_categorical(corpus.train_labels[i], corpus.n_actions)
            pretrained_sentence = get_pretrained_sentence(corpus.train_sentences[i], pretrained_embeddings, opts.pretrained_embeddings_dim)

            n_tokens = len(sentence)
            assert(n_tokens == len(postags) and n_tokens == len(labels) and n_tokens == len(pretrained_sentence))

            LOG.debug("sentence=%s" % sentence)
            LOG.debug("postags=%s" % postags)
            # LOG.debug("labels=%s" % labels)
            LOG.debug("training id=%d sample, n_tokens=%d" % (i, n_tokens))

            history = model.train_on_batch({
                "wid": np.array([sentence]),
                "pid": np.array([postags]),
                "pre_word_emb": np.array([pretrained_sentence]),
                "output": np.array([labels])})
            loss += sum(history)
            if n_processed % 2500 == 0 and n_processed > 0:
                LOG.info("Trained %d instances." % n_processed)
                f_score = evaluate(opts, corpus, vocabulary, singleton, pretrained_embeddings, model, kUNK)
                LOG.info("Iteration %.2f: F-score=%f, loss=%f" % (
                    iteration + float(n_processed) / corpus.n_train, f_score, loss / n_processed))
                if f_score > best_f_score:
                    best_f_score = f_score
                    model.save_weights(fname, overwrite=True)
                    LOG.info("New best score %f is achieved, model saved." % best_f_score)

        f_score = evaluate(opts, corpus, vocabulary, singleton, pretrained_embeddings, model, kUNK)
        LOG.info("Iteration %d: F-score: %f, loss=%f" % (iteration + 1, f_score, loss / len(order)))
        if f_score > best_f_score:
            best_f_score = f_score
            model.save_weights(fname, overwrite=True)
            LOG.info("New best score %f is achieved, model saved." % best_f_score)


def main():
    opts = init_command_line(sys.argv[1:])
    if not hasattr(opts, "training_data"):
        LOG.error("Please specify --training_data (-T): "
                  "this is required to determine the vocabulary mapping, "
                  "even if the parser is used in prediction mode.")
        sys.exit(1)

    assert(0. <= opts.unk_prob <= 1.)

    corpus = Corpus()
    corpus.load_correct_actions(opts.training_data)
    LOG.info("Loaded # sentence: %d" % corpus.n_train)

    vocabulary, singleton = get_vocabulary_and_singleton(corpus)
    LOG.info("Vocabulary size equals %d, singleton size equals %d" % (len(vocabulary), len(singleton)))

    if hasattr(opts, "words"):
        pretrained, opts.pretrained_embeddings_dim = load_pretrained_word_embedding(opts, corpus)
    else:
        pretrained, opts.pretrained_embeddings_dim = None, 0
    LOG.info("# words after loading pretrained word embedding: %d" % corpus.n_words)
    LOG.info("Pre-trained word embedding is loaded with %d dimensions." % opts.pretrained_embeddings_dim)

    builder = LSTMSequenceLabelerBuilder()\
        .set_layers(opts.layers)\
        .set_word_size(corpus.n_words + 1)\
        .set_word_dim(opts.word_dim)\
        .set_postag_size(corpus.n_postags + 10)\
        .set_postag_dim(opts.pos_dim)\
        .set_hidden_dim(opts.hidden_dim)\
        .set_label_size(corpus.n_actions)\
        .set_pretrained_embeddings_dim(opts.pretrained_embeddings_dim)

    if opts.graph == "plain":
        model = builder.build_plain_graph()
    elif opts.graph == "plain_alternative":
        model = builder.build_plain_alternative_graph()
    elif opts.graph == "chris":
        model = builder.set_lstm_input_dim(opts.lstm_input_dim).build_chris_graph()
    elif opts.graph == "seq2seq":
        model = builder.build_seq2seq_graph()
    else:
        raise RuntimeError("Unknown graph type: %s" % opts.graph)
    LOG.info("Computation graph %s is built." % opts.graph)

    model.compile(opts.optimizer, {'output': 'categorical_crossentropy'})
    LOG.info("Computation graph %s is compiled." % opts.graph)

    corpus.load_correct_dev_actions(opts.dev_data)

    if opts.train:
        fname = get_model_name(opts)
        LOG.info("Writing parameter to file %s" % fname)
        LOG.info("Start training.")
        train(opts, fname, corpus, vocabulary, singleton, pretrained, model)
    else:
        fname = opts.model

    kUNK = corpus.get_or_add_word(corpus.UNK)
    model.load_weights(fname)
    LOG.info("Model %s is loaded." % fname)
    f_score = evaluate(opts, corpus, vocabulary, singleton, pretrained, model, kUNK)
    LOG.info("Final F: %f" % f_score)


if __name__ == "__main__":
    main()


#!/usr/bin/env python
from log import get_logger


def indexing(str_to_idx, idx_to_str, str, idx):
    """

    :param dict[str, int] str_to_idx:
    :param dict[int, str] idx_to_str:
    :param str str:
    :param int idx:
    :return:
    """
    str_to_idx[str] = idx
    idx_to_str[idx] = str


class Corpus(object):
    LOG = get_logger(__name__)
    UNK = "UNK"
    BAD0 = "BAD0"
    BOS = "</s>"

    def __init__(self):
        self.USE_SPELLING = False

        self.train_sentences = {}
        self.train_postags = {}
        self.train_labels = {}

        self.dev_sentences = {}
        self.dev_postags = {}
        self.dev_labels = {}

        self.n_train = 0
        self.n_dev = 0
        self.n_words = 0
        self.n_postags = 0
        self.n_chars = 0
        self.n_actions = 0

        self.max_word = 0
        self.max_postag = 0
        self.max_char = 0

        self.word_to_int = {}
        self.int_to_word = {}

        self.pos_to_int  = {}
        self.int_to_pos  = {}

        self.chars_to_int = {}
        self.int_to_chars = {}

        self.actions_to_int = []

    def load_correct_actions(self, filename):
        """

        :param str filename:
        :return:
        """
        indexing(self.word_to_int, self.int_to_word, self.BAD0, 0)
        indexing(self.word_to_int, self.int_to_word, self.UNK, 1)
        indexing(self.word_to_int, self.int_to_word, self.BOS, 2)
        assert(self.max_word == 0)  # word is unset
        self.max_word = 3

        assert(self.max_postag == 0)  # POS is unset
        self.max_postag = 1

        indexing(self.chars_to_int, self.int_to_chars, self.BAD0, 0)
        self.max_char = 1

        self.actions_to_int.append("O")
        self.n_actions = 1
        try:
            dataset = open(filename, "r").read().strip().split("\n\n")
            for sid, data in enumerate(dataset):
                self.train_sentences[sid] = sentence = []
                self.train_postags[sid] = postags = []
                self.train_labels[sid] = labels = []

                for line in data.split("\n"):
                    line = line.strip().replace("-RRB-", "_RRB_").replace("-LRB-", "_LRB_")
                    word, postag, nptag, label = line.split()

                    if postag not in self.pos_to_int:
                        indexing(self.pos_to_int, self.int_to_pos, postag, self.max_postag)
                        self.n_postags = self.max_postag
                        self.max_postag += 1

                    if word not in self.word_to_int:
                        indexing(self.word_to_int, self.int_to_word, word, self.max_word)
                        self.n_words = self.max_word
                        self.max_word += 1

                        for ch in word.decode("utf-8"):
                            if ch not in self.chars_to_int:
                                indexing(self.chars_to_int, self.int_to_chars, ch, self.max_char)
                                self.n_chars = self.max_char
                                self.max_char += 1

                    if label not in self.actions_to_int:
                        self.actions_to_int.append(label)
                        self.n_actions += 1

                    sentence.append(self.word_to_int[word])
                    postags.append(self.pos_to_int[postag])
                    labels.append(self.actions_to_int.index(label))

            self.n_train = len(self.train_sentences)
            self.LOG.info("# labels: %d" % self.n_actions)
            self.LOG.info("# postags: %d" % self.n_postags)
            self.LOG.info("# words: %d" % self.n_words)
        except Exception, e:
            self.LOG.error("Failed to load data from file: %s" % filename)
            raise e

    def load_correct_dev_actions(self, filename):
        """

        :param str filename:
        :return:
        """
        assert(self.max_word > 3)  # word is unset
        assert(self.max_postag > 1)  # POS is unset

        try:
            dataset = open(filename, "r").read().strip().split("\n\n")
            for sid, data in enumerate(dataset):
                self.dev_sentences[sid] = sentence = []
                self.dev_postags[sid] = postags = []
                self.dev_labels[sid] = labels = []

                for line in data.split("\n"):
                    line = line.strip().replace("-RRB-", "_RRB_").replace("-LRB-", "_LRB_")
                    word, postag, nptag, label = line.split()

                    if postag not in self.pos_to_int:
                        indexing(self.pos_to_int, self.int_to_pos, postag, self.max_postag)
                        self.n_postags = self.max_postag
                        self.max_postag += 1

                    if word not in self.word_to_int:
                        if self.USE_SPELLING:
                            indexing(self.word_to_int, self.int_to_word, word, self.max_word)
                            self.n_words = self.max_word
                            self.max_word += 1
                        else:
                            word = self.UNK

                    if label not in self.actions_to_int:
                        msg = "Unexpected: action %s in DEV not appear in TRAIN" % label
                        self.LOG.error(msg)
                        raise RuntimeError(msg)

                    sentence.append(self.word_to_int[word])
                    postags.append(self.pos_to_int[postag])
                    labels.append(self.actions_to_int.index(label))
            self.n_dev = len(self.dev_sentences)
        except Exception, e:
            self.LOG.error("Failed to load data from file: %s" % filename)
            raise e

    def get_or_add_word(self, word):
        """

        :param str word:
        :return:
        """
        if word not in self.word_to_int:
            indexing(self.word_to_int, self.int_to_word, word, self.max_word)
            self.max_word += 1
            self.n_words = self.max_word
        return self.word_to_int[word]

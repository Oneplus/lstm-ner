#!/usr/bin/env python
import logging
import sys

DEFAULT_LEVEL = 'noset'
FORMAT = '%(asctime)-15s %(levelname)s [%(name)s]: %(message)s'


def get_logger(name, filename=None, level=DEFAULT_LEVEL):
    logger = logging.getLogger(name)
    if filename is None:
        handler = logging.StreamHandler(sys.stderr)
    else:
        handler = logging.StreamHandler(open(filename, "w"))
    handler.setFormatter(logging.Formatter(FORMAT))
    logger.addHandler(handler)

    if level == "error":
        logger.setLevel(logging.ERROR)
    elif level == "warn":
        logger.setLevel(logging.WARN)
    elif level == "info":
        logger.setLevel(logging.INFO)
    elif level == "debug":
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.DEBUG)

    return logger
